'''
A simple card game simulator
'''

from deck import Deck


# This function can be helpful for debugging. It prints out a list of cards
# that are dealt to a player
def print_hand(hand):
    for card in hand:
        print(card.__str__())


def show_result(hi, lo):
    print(hi.get_rank(), "of", hi.get_suit(), "beats", lo.get_rank(), "of", lo.get_suit())


def main():
    deck = Deck()
    deck.shuffle()

    # Set up a two-player game (this can easily be expanded to more players)
    NUM_PLAYERS = 2
    player_cards = []
    player_scores = []

    for i in range(NUM_PLAYERS):
        player_cards.append([])
        player_scores.append(0)

    # Deal all the cards to the players
    while len(deck.cards) > 0:
        for j in range(NUM_PLAYERS):
            try:
                player_cards[j].append(deck.deal_card())
            except IndexError:
                break

    # The deck should now be empty
    print("There are", len(deck.cards), "cards in the deck now")

    # Play the game. Each player shows one card. The player with the higher ranking card
    # scores a point. If both cards have the same rank, no point is awarded to either player.
    # Cards are returned to the deck after they are played. The player with the most points
    # at the end wins the game. The rest of the code was designed specifically for a two-player
    # game, so you will need to think carefully about how to make adjustments for single
    # player or more than two players.

    while len(player_cards[0]) > 0:
        # Each player shows a card
        card0 = player_cards[0].pop()
        card1 = player_cards[1].pop()

        if card0.get_int_rank() > card1.get_int_rank():
            player_scores[0] += 1
            show_result(card0, card1)
            print("Player 1 scores a point.")

        elif card1.get_int_rank() > card0.get_int_rank():
            player_scores[1] += 1
            show_result(card1, card0)
            print("Player 2 scores a point.")

        else:
            print(card0.__str__(), "ties with", card1.__str__(), "\nNo one scores a point")

        print("There are", len(player_cards[0]), "cards left.\n")

        deck.return_to_deck([card0, card1])

    # Show the game results
    print("Player 1 score:", player_scores[0], "   Player 2 score:", player_scores[1])

    if player_scores[0] > player_scores[1]:
        print("Player 1 wins!")
    elif player_scores[1] > player_scores[0]:
        print("Player 2 wins!")
    else:
        print("It's a tie!")

    print("\n\nNumber of cards back in the deck: ", len(deck.cards), "\n")
    print(deck)


main()
