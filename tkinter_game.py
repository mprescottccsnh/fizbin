from tkinter import *
from deck import *
from poker import *

class MainGUI:

    def __init__(self):
        window = Tk()
        window.title("Poker Hand Analyzer")

        self.card_labels = list()   # will hold the Label objects for displaying the cards
        self.num_cards = 5          # how many cards per hand

        self.deck = Deck()
        self.deck.set_poker_tester()
        #print(self.deck)

        self.hand_value = StringVar()
        self.hand_value.set("Hit Deal to get the next hand\n")

        self.tableau = Frame(window)
        self.back_of_card = PhotoImage(file="cards/b1fv.gif")

        for i in range(self.num_cards):
            label = Label(self.tableau, image=self.back_of_card)
            self.card_labels.append(label)
            label.pack(side=LEFT)

        self.tableau.pack()

        self.controller = Frame(window)
        self.controller.pack()

        self.deal_button = Button(self.controller, text="Deal", command=self.deal).pack()
        self.hand_rank = Label(self.controller, textvariable=self.hand_value).pack()

        window.mainloop()

    def deal(self):
        if len(self.deck.cards) == 0:
            self.deck.reset()
            self.deck.shuffle()

        h = list()

        for i in range(self.num_cards):
            try:
                next_card = self.deck.deal_card()
                h.append(next_card)
                self.card_labels[i].config(image=self.deck.card_images[next_card.value])

            except IndexError:
                h = list()
                for x in range(self.num_cards):
                    self.card_labels[x].config(image=self.back_of_card)
                break

        # analyze the hand
        self.hand_value.set(analyze(h))

MainGUI()
