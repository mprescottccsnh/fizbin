from random import shuffle
from card import *
from tkinter import PhotoImage


class Deck:

    def __init__(self, no_jokers=True, gui=True):
        self.cards = list()

        # Initially build the deck to include the jokers
        for i in range(deck_size+2):
            self.cards.append(Card(i))

        # If GUI is needed, put all the card images (including the jokers)
        # into a tuple of PhotoImage objects

        if gui:
            images = []
            for x in self.cards:
                try:
                    images.append(PhotoImage(file=x.get_image()))
                except RuntimeError:
                    break
            self.card_images = tuple(images)

        # Remove jokers as needed
        if no_jokers:
            self.cards.pop()
            self.cards.pop(0)

    def __str__(self):
        card_str = ''
        for card in self.cards:
            card_str += card.__str__() + '\n'
        return card_str

    def shuffle(self):
        shuffle(self.cards)
        return

    def empty_the_deck(self):
        self.cards = list()
        return

    def deal_card(self):
        return self.cards.pop(0)

    def return_to_deck(self, cards):
        for card in cards:
            self.cards.append(card)
        self.shuffle()
        return

    def is_empty(self):
        if len(self.cards) == 0:
            return True
        return False

    def reset(self, no_jokers=True):
        self.cards = list()

        # Initially build the deck to include the jokers
        for i in range(deck_size+2):
            self.cards.append(Card(i))

        # Remove jokers as needed
        if no_jokers:
            self.cards.pop()
            self.cards.pop(0)

        return

    def set_poker_tester(self):
        values = [
            2,15,29,42,43, 3,16,17,31,45, 5,18,44,30,45,
            6,20,34,35,49, 40,41,46,47,48, 9,22,8,4,7,
            1,10,11,12,13, 14,15,19,21,23, 24,25,26,27,28,
            32,33,36,37,38, 39,45

        ]

        self.cards = list()

        for x in values:
            self.cards.append(Card(x))

        return
