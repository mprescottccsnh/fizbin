# this module contains functions for determining the value of a hand of poker
# refer to this web site for a poker hand algorithm:
# http://nsayer.blogspot.com/2007/07/algorithm-for-evaluating-poker-hands.html
#
hand = None
by_rank = None
by_suit = None


def is_royal_flush():
    global by_rank, by_suit
    if len(by_suit) == 1\
            and by_rank == [(12, 1), (11, 1), (10, 1), (9, 1), (0, 1)]:
        return True
    return False

#-----------------------------------#

def is_straight_flush():
    global by_rank, by_suit
    if len(by_suit) == 1\
            and len(by_rank) == 5\
            and (by_rank[0][0] - by_rank[4][0]) == 4:
        return True
    return False

#-----------------------------------#

def is_four_of_a_kind():
    global by_rank, by_suit
    for rank in by_rank:
        if rank[1] == 4:
            return True
    return False

#-----------------------------------#

def is_full_house():
    global by_rank, by_suit
    if len(by_rank) == 2 and by_rank[0][0] == 3 and by_rank[1][0] == 2:
        return True
    return False

#-----------------------------------#

def is_flush():
    global by_rank, by_suit
    if len(by_suit) == 1:
        return True
    return False

#-----------------------------------#

def is_straight():
    global by_rank, by_suit
    if len(by_rank) == 5 \
            and (by_rank[0][0] - by_rank[4][0]) == 4:
        return True
    return False


#-----------------------------------#

def is_three_of_a_kind():
    global by_rank, by_suit

    if len(by_rank) == 3 and by_rank[0][0] == 3:
        return True
    return False


#-----------------------------------#

def is_two_pairs():
    global by_rank, by_suit
    if len(by_rank) == 3 and by_rank[0][0] == 2:
        return True
    return False


#-----------------------------------#
def is_pair():
    global by_rank, by_suit
    if len(by_rank) == 4:
        return True
    return False


#-----------------------------------#
def analyze(h):
    global hand, by_suit, by_rank

    hand = h
    histogram_r = dict()
    histogram_s = dict()

    for card in hand:
        try:
            histogram_r[card.get_int_rank()] += 1
        except KeyError:
            histogram_r[card.get_int_rank()] = 1

        try:
            histogram_s[card.get_int_suit()] += 1
        except KeyError:
            histogram_s[card.get_int_suit()] = 1

    by_rank = [(k, v) for v, k in sorted([(v, k) for k, v in histogram_r.items()], reverse=True)]
    by_suit = [(k, v) for v, k in sorted([(v, k) for k, v in histogram_s.items()], reverse=True)]

    print(by_rank)

    if is_royal_flush():
        return "Royal Flush"
    elif is_straight_flush():
        return "Straight Flush"
    elif is_four_of_a_kind():
        return "4 of a Kind"
    elif is_full_house():
        return "Full House"
    elif is_flush():
        return "Flush"
    elif is_straight():
        return "Straight"
    elif is_three_of_a_kind():
        return "3 of a kind"
    elif is_two_pairs():
        return "2 pairs"
    elif is_pair():
        return "Pair"
    else:
        return "Nothing"

