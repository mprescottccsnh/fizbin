from lib import *


# A class to represent a standard playing card
class Card:
    # Initializer (constructor)
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return self.get_rank() + " of " + self.get_suit()

    # Return the suit of the card as a string
    def get_suit(self):
        if self.value <= 0:
            return "Red"
        elif self.value > deck_size:
            return "Black"
        return suits[(self.value-1) // len(ranks)]

    # Return the face value of the card as a string
    def get_rank(self):
        if self.value <= 0 or self.value > deck_size:
            return "Joker"
        return ranks[(self.value-1) % len(ranks)]

    # Return the integer value for the rank
    def get_int_rank(self):
        if self.value <= 0 or self.value > deck_size:
            return
        return (self.value-1) % len(ranks)

    # Return the integer value for the suit
    def get_int_suit(self):
        if self.value <= 0 or self.value > deck_size:
            return
        return (self.value-1) // len(ranks)

    # Return the path to the image file for this card
    def get_image(self):
        return "cards/" + str(self.value) + ".gif"
